# Tryton installation and maintainance scripts for PIP

This little collection of scripts to install and maintain the fabulous "Tryton" ERP system server on Ubuntu, Debian and Manjaro linux in python virtual environments moved to: 

[Tryton community](https://foss.heptapod.net/tryton-community/tools/tryton-installation-and-maintainance-scripts-for-pip-venv)

Please do us the honour to visit us there.

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Diese kleine Skriptsammlung zur Installation und Verwaltung der fabelhaften Unternehmenssoftware(ERP) "Tryton" auf  Ubuntu, Debian and Manjaro linux in virtuellen Umgebungen von Python ist hierher umgezogen: 

[Tryton community](https://foss.heptapod.net/tryton-community/tools/tryton-installation-and-maintainance-scripts-for-pip-venv)

Bitte erweisen Sie uns die Ehre und besuchen uns dort.

